use [triger_fk_cursor]

go 

insert into [zone]([name]) values ('head'),
								  ('stomach'),
								  ('eyes'),
								  ('teeth'),
								   ('nose')
go
insert into [street]([street]) values('Zelena'),
                                  ('Chuprynky'),
								  ('Shevchenka'),
								  ('Horodotska'),
								  ('Kuchera')
go
insert into [post]([post]) values ('pharmacist'),
                                  ('manager')
go
insert into [pharmacy]([name],[building_number],[www],[street])values ('pharmacy1','107','www.pharmacy1.com','Zelena'),
                                                                      ('pharmacy2','40','www.pharmacy2.com','Chuprynky'),
																	  ('pharmacy3','56','www.pharmacy3.com','Shevchenka'),
																	  ('pharmacy4','37','www.pharmacy4.com','Horodotska'),
																	  ('pharmacy5','3','www.pharmacy5.com','Kuchera')
go
insert into [employee]([surname],[name],[middle_name],[passport],[identity_number],[experience],[birthday],[post],[pharmacy_id])
values('Mazur','Yuriy','Antonovich','KB10764556','1212345690',5,'1991-05-16','manager',1),
      ('Kharkhalis','Vasylyna','Ihorivna','KC10788582','5311987490',6,'1992-10-25','pharmacist',2),
	  ('Maherovska','Olena','Ivanivna','KC10788582','0221987490',100,'1851-05-16','manager',1),
	  ('Malanchuk','Orest','Pulupovich','KC12758951','0225256290',10,'1990-10-26','manager',4),
   	 ('Mukich','Khrystyna','Vasylivna','KO12757751','1225223590',30,'1965-05-10','pharmacist',2)
go																																 
insert into [medicine]([name],[ministry_code]) values ('Kopatcyl','OH-111-00'),
                                                     ('Mezym','OH-111-01'),
													 ('Noshpa','OH-111-02'),
													 ('Evkazolin','OH-111-03'),
													 ('Sensodyn','OH-111-04')													 
go
insert into [medicine_zone](medicine_id,zone_id) values (1,1),(2,2),(4,3), (5,4)
                                                         (3,1),(3,2),(3,3),(3,4),(3,5)
go
insert into [pharmacy_medicine](pharmacy_id,medicine_id) values (1,1),(1,2),(1,3),(1,4),(1,5),
                                                                (2,1),(2,2),(2,3),(2,4),(2,5),
																(3,1),(3,2),(3,3),(3,4),(3,5),
																(4,1),(4,2),(4,3),(4,4),(4,5),
																(5,1),(5,2),(5,3),(5,4),(5,5)
go