USE  [triger_fk_cursor] 
GO

drop trigger if exists [trg_delete_street]
go
create trigger [trg_delete_street]
on [street]
after delete
as
begin
UPDATE pharmacy SET street=NULL
where street in (SELECT street from DELETED)
end

/*
drop trigger if exists [trg_delete_post]
go
create trigger [trg_delete_post]
on [post]
after delete
as
begin
if
(select count(*) from employee
where post in (SELECT post from DELETED))>0 
begin 
PRINT 'Error: we have employee with this post'
ROLLBACK TRANSACTION
end
end
*/

drop trigger if exists [trg_delete_pharmacy]
go
create trigger [trg_delete_pharmacy]
on [pharmacy]
after delete
as
begin
UPDATE employee SET pharmacy_id=NULL
where pharmacy_id in (SELECT pharmacy_id from DELETED)
DELETE from pharmacy_medicine 
where pharmacy_id in (SELECT pharmacy_id from DELETED)
end

drop trigger if exists [trg_delete_pharmacy_medicine]
go
create trigger [trg_delete_pharmacy_medicine]
on [medicine]
after delete
as
begin
DELETE from pharmacy_medicine 
where medicine_id in (SELECT medicine_id from DELETED)
DELETE from medicine_zone 
where medicine_id in (SELECT medicine_id from DELETED)
end

drop trigger if exists [trg_delete_zone]
go
create trigger [trg_delete_zone]
on [zone]
after delete
as
begin
if
(select count(*) from medicine_zone
where zone_id in (SELECT zone_id from DELETED))>0 
begin 
PRINT 'Error: we have medicine with this zone'
ROLLBACK TRANSACTION
end
end

/*
drop trigger if exists [trg_update_post]
go
create trigger [trg_update_post]
on [post]
after update
as
begin
update employee set post=(select post from INSERTED) 
where post in (select post from DELETED)
end
*/

drop trigger if exists [trg_update_street]
go
create trigger [trg_update_street]
on [street]
after update
as
begin
update pharmacy set street=(select street from INSERTED) 
where street in (select street from DELETED)
end

drop trigger if exists [trg_insert_employee]
go
create trigger [trg_insert_employee]
on [employee]
after insert, update
as
begin
if (select identity_number from INSERTED) like '%00'
ROLLBACK TRANSACTION
end

drop trigger if exists [trg_insert_medicine]
go
create trigger [trg_insert_medicine]
on [medicine]
after insert, update
as
begin
if (select ministry_code from INSERTED) NOT like '[a-z^mp][a-z^mp]-[0-9][0-9][0-9]-[0-9][0-9]'
ROLLBACK TRANSACTION
end

drop trigger if exists [trg_modify_post]
go
create trigger [trg_modify_post]
on [post]
after insert, update, delete
as
begin
PRINT 'Error: modification of this table is restricted'
ROLLBACK TRANSACTION
end