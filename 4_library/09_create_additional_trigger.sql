USE  [V_Kharkhalis_library] 
GO

create trigger [trg_deletion]
on dbo.authors_log
after delete
as
IF EXISTS (SELECT * FROM deleted)
BEGIN
	PRINT 'Error delete'
	ROLLBACK TRANSACTION
END
go