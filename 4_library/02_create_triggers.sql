USE  [V_Kharkhalis_library] 
GO

----create triggers-----------------------------------------------

drop trigger if exists [tr_authors_log_insert],[trg_update_authors],[trg_update_books],[trg_update_authors_books],[trg_update_publisher],
[tr_authors_log_update],[tr_authors_log_delete]
go 

/*create trigger [trg_update_authors]
on dbo.authors
after update
as
begin
declare @user varchar(10) = system_user
declare @date datetime = GETDATE()
UPDATE authors
set updated = @date, updated_by = @user
where author_id in (select author_id from INSERTED)
end
go
*/

create trigger [trg_update_books]
on dbo.books
after update
as
begin
declare @user varchar(10) = system_user
declare @date datetime = GETDATE()
UPDATE books
set updated = @date, updated_by = @user
where ISBN in (select ISBN from INSERTED)
end
go

create trigger [trg_update_authors_books]
on dbo.booksauthors
after update
as
begin
declare @user varchar(10) = system_user
declare @date datetime = GETDATE()
UPDATE booksauthors
set updated = @date, updated_by = @user
where BooksAuthors_id in (select BooksAuthors_id from INSERTED)
end
go

create trigger [trg_update_publisher]
on dbo.publishers
after update
as
begin
declare @user varchar(10) = system_user
declare @date datetime = GETDATE()
UPDATE publishers
set updated = @date, updated_by = @user
where publisher_id in (select publisher_id from INSERTED)
end
go

go
create trigger [tr_authors_log_insert]
on dbo.authors
after insert
as
BEGIN 
   declare @user varchar(10) = system_user
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'I'

	 insert into Authors_log(author_id_new,name_new,URL_new,operation_type,operation_datetime)  
	        (select author_id,name,URL,@type,@date from INSERTED)
end
go

create trigger tr_authors_log_delete
on dbo.authors
after delete
as
BEGIN 
   declare @user varchar(10) = system_user
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'D'

	 insert into Authors_log(author_id_old,name_old,URL_old,operation_type,operation_datetime)    
	        (select author_id,name,URL,@type,@date from DELETED) 
	end
go

create trigger tr_authors_log_update
on dbo.authors
after update
as
BEGIN 
   declare @user varchar(10) = system_user
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'U'

	update dbo.authors
	set updated = GETDATE(), updated_by = @user
	where author_id in (select author_id from inserted)
	insert into Authors_log(author_id_new,name_new,URL_new,author_id_old,name_old,URL_old,operation_type,operation_datetime)
	select i.author_id,i.name,i.URL,d.author_id,d.name,d.URL,@type,@date from inserted i,deleted d
	end
			