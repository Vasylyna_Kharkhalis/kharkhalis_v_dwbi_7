use [V_Kharkhalis_library] 


delete [Books]
where [price] > 200

go
delete [Authors]
where [author_id] < 5
 
go 

delete [Publishers]
where [publisher_id] = 6

go 

delete [BooksAuthors]
where [ISBN] = NULL or [author_id] = NULL

go
