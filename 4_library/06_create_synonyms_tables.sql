use V_Kharkhalis_library_synonym
go
--- create synonyms-----------------------------------
create synonym authors for V_Kharkhalis_library.dbo.authors
create synonym books for V_Kharkhalis_library.dbo.books
create synonym booksauthors for V_Kharkhalis_library.dbo.booksauthors
create synonym publishers for V_Kharkhalis_library.dbo.publishers
create synonym authors_log for V_Kharkhalis_library.dbo.authors_log
go