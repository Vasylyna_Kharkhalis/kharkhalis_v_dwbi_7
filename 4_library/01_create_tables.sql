use [master]
go
drop database if exists [V_Kharkhalis_library]
go
create database [V_Kharkhalis_library]
go
ALTER DATABASE [V_Kharkhalis_library] ADD FILEGROUP [Data]
GO
/*create new data file */

ALTER DATABASE [V_Kharkhalis_library] 
ADD FILE ( NAME = N'V_Kharkhalis_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\V_Kharkhalis_library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO

/*set default file group*/
USE  [V_Kharkhalis_library] 
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	ALTER DATABASE [V_Kharkhalis_library]  MODIFY FILEGROUP [Data] DEFAULT
GO

use [V_Kharkhalis_library]
go
drop table if exists [dbo].[authors]
 GO

CREATE TABLE [dbo].[authors]
(
 [author_id]       BIGINT IDENTITY (1, 1) NOT NULL ,
 [name] VARCHAR(40) NOT NULL ,
  [URL]	VARCHAR(255) NOT NULL DEFAULT 'www.author_name.com',
 [inserted]	  DATETIME NOT NULL CONSTRAINT [author1] DEFAULT getdate(),
 [inserted_by]	VARCHAR(40)  NOT NULL CONSTRAINT [author2] DEFAULT system_user,
  [updated] DATETIME NULL,
  [updated_by]	VARCHAR(40) NULL ,

 CONSTRAINT [PK_authors] PRIMARY KEY CLUSTERED ([author_id] ASC),
 CONSTRAINT [UK_name] UNIQUE NONCLUSTERED ([name] ASC),
 );
 GO

 drop table if exists [dbo].[books]
 GO

CREATE TABLE [dbo].[books]
(
 [ISBN]	VARCHAR(255) NOT NULL ,
 [Publisher_Id]	BIGINT NOT NULL,
 [URL]	VARCHAR(255) NOT NULL ,
 [Price] MONEY NOT NULL  CONSTRAINT [author3] DEFAULT 0,
 [inserted]	  DATETIME NOT NULL CONSTRAINT [author4] DEFAULT getdate(),
 [inserted_by]	VARCHAR(40)  NOT NULL CONSTRAINT [author5] DEFAULT system_user,
  [updated] DATETIME NULL,
  [updated_by]	VARCHAR(40) NULL ,


 CONSTRAINT [PK_books] PRIMARY KEY CLUSTERED ([ISBN] ASC),
 CONSTRAINT [UK_URL_b] UNIQUE NONCLUSTERED ([URL] ASC),
);
GO

drop table if exists [dbo].[BooksAuthors]
 GO

CREATE TABLE [dbo].[BooksAuthors]
(
[BooksAuthors_id]  BIGINT IDENTITY (1, 1) NOT NULL ,
[ISBN]  VARCHAR(255) NOT NULL ,
[Author_Id] BIGINT NOT NULL ,
[Seq_No]    INT NOT NULL CONSTRAINT [author6] DEFAULT 1,
 [inserted]	  DATETIME NOT NULL CONSTRAINT [author7] DEFAULT getdate(),
 [inserted_by]	VARCHAR(40)  NOT NULL CONSTRAINT [author8] DEFAULT system_user,
  [updated] DATETIME NULL,
  [updated_by]	VARCHAR(40) NULL ,


 CONSTRAINT [PK_booksAuthors] PRIMARY KEY CLUSTERED ([BooksAuthors_id] ASC),
 CONSTRAINT [UK_author_id] UNIQUE NONCLUSTERED ([ISBN] ASC, [Author_Id] ASC),
);
GO

 drop table if exists [dbo].[publishers]
 GO

CREATE TABLE [dbo].[publishers]
(
 [publisher_id]  BIGINT IDENTITY (1, 1) NOT NULL ,
 [Name] VARCHAR(140) NOT NULL ,
 [URL] VARCHAR(255) NOT NULL DEFAULT 'www.publishers_name.com',
 [inserted]	  DATETIME NOT NULL CONSTRAINT [author9] DEFAULT getdate(),
 [inserted_by]	VARCHAR(40)  NOT NULL CONSTRAINT [author10] DEFAULT system_user,
  [updated] DATETIME NULL,
  [updated_by]	VARCHAR(40) NULL ,


 CONSTRAINT [pk_publishers] PRIMARY KEY CLUSTERED ([Publisher_Id] ASC),
 CONSTRAINT [UK_URL_pub] UNIQUE NONCLUSTERED ([URL] ASC)
);
GO

 drop table if exists [dbo].[authors_log]
 GO

CREATE TABLE [dbo].[authors_log]
(
 [operation_id] BIGINT IDENTITY (1, 1) NOT NULL ,
 [Author_Id_new] INT NULL ,
 [Name_new] VARCHAR(100) NULL ,
 [URL_new]  VARCHAR(255) NULL ,
 [Author_Id_old]  INT NULL ,
 [Name_old]  VARCHAR(100) NULL ,
 [URL_old]   VARCHAR(255) NULL ,
 [operation_type] VARCHAR(40) NOT NULL ,
 [operation_datetime] DATETIME NOT NULL CONSTRAINT [author11] DEFAULT getdate(),

 CONSTRAINT [PK_authors_log] PRIMARY KEY CLUSTERED ([operation_id] ASC)
 );
GO

------------------------------------- foreign key creation area  ------------------------------------------------


ALTER TABLE [dbo].[books]  
ADD
CONSTRAINT [fk_books_publisher_id] FOREIGN KEY ([publisher_id])
REFERENCES [dbo].[publishers]([publisher_id])

ALTER TABLE [dbo].[BooksAuthors]  
ADD
CONSTRAINT [fk_book_ISBN] FOREIGN KEY ([ISBN])
REFERENCES [dbo].[books]([ISBN])

ALTER TABLE [dbo].[BooksAuthors] 
ADD CONSTRAINT [fk_BOOK_author] FOREIGN KEY ([author_id])
REFERENCES [dbo].[authors]([author_id])

