use V_Kharkhalis_library_view
go
--- create views-----------------------------------
CREATE OR ALTER VIEW dbo.authors_view
(
      [author_id],
      [name],
      [URL],
      [inserted],
      [inserted_by],
      [updated],
      [updated_by]
  )
  AS
  SELECT [author_id],
       [name],
      [URL],
      [inserted],
      [inserted_by],
      [updated],
      [updated_by]
FROM [V_Kharkhalis_library].[dbo].[authors]
go

CREATE OR ALTER VIEW dbo.books_view
(
      [ISBN]
      ,[Publisher_Id]
      ,[URL]
      ,[Price]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by])
  AS
  SELECT [ISBN]
      ,[Publisher_Id]
      ,[URL]
      ,[Price]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by]
  FROM [V_Kharkhalis_library].[dbo].[books]
go

CREATE OR ALTER VIEW dbo.BooksAuthors_view
(
      [BooksAuthors_id]
      ,[ISBN]
      ,[Author_Id]
      ,[Seq_No]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by])
  AS
  SELECT [BooksAuthors_id]
      ,[ISBN]
      ,[Author_Id]
      ,[Seq_No]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by]
  FROM [V_Kharkhalis_library].[dbo].[BooksAuthors]
  go

  CREATE OR ALTER VIEW dbo.publishers_view
(
      [publisher_id]
      ,[Name]
      ,[URL]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by])
  AS
  SELECT [publisher_id]
      ,[Name]
      ,[URL]
      ,[inserted]
      ,[inserted_by]
      ,[updated]
      ,[updated_by]
  FROM [V_Kharkhalis_library].[dbo].[publishers]
go

CREATE OR ALTER VIEW dbo.authors_log_view
(
      [operation_id]
      ,[Author_Id_new]
      ,[Name_new]
      ,[URL_new]
      ,[Author_Id_old]
      ,[Name_old]
      ,[URL_old]
      ,[operation_type]
      ,[operation_datetime])
  AS
  SELECT [operation_id]
      ,[Author_Id_new]
      ,[Name_new]
      ,[URL_new]
      ,[Author_Id_old]
      ,[Name_old]
      ,[URL_old]
      ,[operation_type]
      ,[operation_datetime]
  FROM [V_Kharkhalis_library].[dbo].[authors_log]
