----------------1-----------
;with CTE AS (SELECT hd FROM laptop)
SELECT * FROM CTE

----------------2-----------
;with CTE AS (SELECT hd FROM laptop),
CTE_2 AS (SELECT hd FROM CTE WHERE hd>10)
SELECT * FROM CTE_2

----------------5-----------
; WITH RowCTE(row_no) AS  
(  
SELECT row_no = 1    
UNION ALL
SELECT  row_no + 1 FROM RowCTE WHERE row_no < 10000
)  
 
SELECT * FROM RowCTE
OPTION ( MAXRECURSION 10000)

----------------6-----------
; WITH RowCTE(row_no) AS  
(  
SELECT row_no = 1    
UNION ALL
SELECT  row_no + 1 FROM RowCTE WHERE row_no < 100000
)  
 
SELECT * FROM RowCTE
OPTION ( MAXRECURSION 0)

---------------8------------
SELECT DISTINCT maker FROM product
WHERE type='pc' AND maker NOT IN (SELECT maker FROM product
WHERE type='laptop')

---------------9------------
SELECT DISTINCT maker FROM product
WHERE type='pc' AND maker <> ALL (SELECT maker FROM product
WHERE type='laptop')

---------------10-----------
SELECT DISTINCT maker FROM product
WHERE (maker = ANY(SELECT maker FROM Product WHERE type='pc'))
AND maker NOT IN (SELECT maker FROM Product WHERE type ='laptop')

---------------11-----------
SELECT DISTINCT maker FROM product
WHERE type='pc' AND maker IN (SELECT maker FROM product
WHERE type='laptop')

---------------12-----------
SELECT DISTINCT maker FROM product
WHERE maker = ALL(SELECT maker FROM Product WHERE type='pc')
AND maker IN (SELECT maker FROM Product WHERE type ='laptop')

---------------13-----------
SELECT DISTINCT maker FROM product
WHERE (maker = ANY(SELECT maker FROM Product WHERE type='pc'))
AND maker IN (SELECT maker FROM Product WHERE type ='laptop')

---------------14-----------
SELECT DISTINCT maker FROM product
WHERE model IN (SELECT DISTINCT model FROM pc)

---------------15-----------
SELECT DISTINCT country, class FROM classes
WHERE class = ALL (select  class FROM classes 
				   WHERE country like 'Ukraine')

--------------16------------
;with cte as (select ship, battle from outcomes where result='damaged')
SELECT DISTINCT C1.ship, C1.battle, C2.date 
FROM outcomes C1 JOIN battles C2
ON C2.name=C1.battle
JOIN cte C3 on C3.ship=C1.ship

--------------17-------------
SELECT DISTINCT maker FROM product C1
WHERE 
EXISTS (SELECT model FROM PC C2 WHERE C1.model=C2.model)

--------------18-------------
 SELECT DISTINCT maker FROM product
WHERE type='pc' AND maker IN (SELECT maker FROM product
WHERE type='printer')and model in (select model from pc
  group by model
 having max(speed)=(select max(speed) from pc))

 --------------19-------------
 SELECT class from ships C1 JOIN outcomes C2 ON C1.name=C2.ship
 WHERE C2.result='sunk'

 --------------20-------------
  SELECT DISTINCT model,price FROM printer
WHERE model in (select model from printer
  group by model
 having max(price)=(select max(price) from printer))

 --------------21-------------
 SELECT L2.type, L1.model, speed from laptop L1 JOIN product L2 on L1.model=L2.model
 WHERE speed<ALL (select speed from pc)

 --------------22------------
 SELECT C2.maker, C1.price FROM printer C1 JOIN product C2 ON C1.model=C2.model
 WHERE C1.color='y' and C1.price=(SELECT MIN(price) from printer where color='y')
  
---------------23------------
;with cte as (select battle, country, count(name) over (partition by country, battle) as number_of_ships
from ships S1 join outcomes S2 on S1.name=S2.ship 
              join classes S3 ON S1.class =S3.class) select distinct * from cte
			  where number_of_ships>1
	
-------------24-------------
;with cte_pc as (select maker, count(pc.model) as pc, '' as laptop, '' as printer 
from product p1 join pc on p1.model=pc.model group by maker),
 cte_laptop as (select maker, count(p2.model) as laptop, '' as pc, '' as printer 
from product p1  join laptop p2 on p1.model=p2.model group by maker),
 cte_printer as (select maker, count(p3.model) as printer, '' as laptop, '' as pc 
from product p1 join printer p3 on p1.model=p3.model group by maker) 
 select cte_pc.maker, cte_pc.pc, cte_laptop.laptop, cte_printer.printer from cte_pc left join cte_laptop on  cte_pc.maker=cte_laptop.maker
                      left join cte_printer on cte_pc.maker=cte_printer.maker         
               
------------25-------------
;with cte_pc as (select maker, count(pc.model) as pc, '' as laptop, '' as printer 
from product p1 join pc on p1.model=pc.model group by maker)
 select cte_pc.maker from product 
 left join cte_pc on  product.maker=cte_pc.maker
               
------------26-------------
select  I1.point, I1.date, isNULL(I2.inc,0), isNULL(I1.out,0)
from outcome_o I1 left join income_o I2 on I1.point=I2.point and I1.date=I2.date

------------27--------------
select name, numGuns, bore, displacement, type, country, launched, C1.class
from classes C1 join ships C2 on C1.class=C2.class
where (select (CASE when numGuns = 8 then 1 else 0 END) +
	          (CASE when bore = 15 then 1 else 0 END) + 
		      (CASE when displacement = 32000 then 1 else 0 END) + 
		      (CASE when type = 'bb' then 1 else 0 END) + 
		      (CASE when country = 'USA' then 1 else 0 END) + 
		      (CASE when launched = 1915 then 1 else 0 END) + 
		      (CASE when C1.class = 'Kon' then 1 else 0 END)) >= 4

-----------29---------------
select maker, pc.model, pc.type, P.price
from     (select model, price from pc
          union all
		  select model, price from printer
		  union all
		  select model, price from laptop) 
P join (select * from product where maker = 'B' ) pc
 on P.model = pc.model

 ----------30---------------
select name, class from ships where class = name
union
select ship, class as name from classes, outcomes 
where classes.class = outcomes.ship

-----------31---------------
select class, count(*) as sum
from (select name, class from ships
       union 
      select OUT.ship as name, CL.class 
      from   outcomes OUT join classes CL 
	  on OUT.ship = CL.class) as ships
group by class
having count(*) = 1

-----------32---------------
select name from ships
 where launched < 1942
union 
select distinct ship 
from outcomes OUT join battles B on OUT.battle=B.name
where YEAR(B.date)<=1942


