Use [V_KH_module_3]
go

--- create views
CREATE OR ALTER VIEW dbo.product_view
(
       product_name, 
       product_photo,
	   product_description,
       price,
	   expiration_date
  )
  AS
  SELECT product_name, 
       product_photo,
	   product_description,
       price,
	   expiration_date
  FROM dbo.product 
WHERE product_name = 'Gayka'
  WITH CHECK OPTION
  go