Use [V_KH_module_3]
go

--- create views
CREATE OR ALTER VIEW dbo.product_view
(
       product_name, 
       product_photo,
	   product_description,
       price,
	   expiration_date
  )
  AS
  SELECT product_name, 
       product_photo,
	   product_description,
       price,
	   expiration_date
  FROM dbo.product 
go

CREATE OR ALTER VIEW dbo.provider_view
(
       name , 
       phone,
       address_id,
	   boss_id,
	   code_edrpou
  )
  AS
  SELECT name , 
       phone,
       address_id,
	   boss_id,
	   code_edrpou
  FROM dbo.provider 
go

CREATE OR ALTER VIEW dbo.supply_view
(
       number_bill,
       [date],
	   product_id, 
       price_incoming,
       quantity
  )
  AS
  SELECT number_bill,
       [date],
	   product_id, 
       price_incoming,
       quantity
  FROM dbo.supply 
go

