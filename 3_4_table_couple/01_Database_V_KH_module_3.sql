use master
go
drop database if exists [V_KH_module_3]
go

CREATE DATABASE [V_KH_module_3]
go

use V_KH_module_3
go

drop table if exists dbo.product
go

CREATE TABLE dbo.product (
       product_id          bigint IDENTITY (1,1)  NOT NULL ,
       product_name        varchar(50) NOT NULL,
       product_photo       image        NULL,
       product_description varchar(250) NULL,
	   container_id        tinyint     NOT NULL,
	   measure_id          tinyint     NOT NULL,
	   product_type_id     int         NOT NULL,
	   expiration_date     date          NULL,
	   trademark_id        int           NULL,
	   price               money       NOT NULL,
	   status_id           tinyint     NOT NULL,
	   inserted_date        datetime not null default getdate(),
	   updated_date        datetime not null default getdate(),

--- define check constraint (row level constraint)
	   constraint product_price_chk check (price >0) ,

----define primary key (table level constraint)
	   constraint product_pk primary key clustered (product_id),

----define unique key (alternate key, table level constraint)
	   constraint product_uk unique (product_name, trademark_id)
)
go

drop table if exists dbo.provider
go

CREATE TABLE dbo.provider (
       provider_id    int  IDENTITY (1,1)  NOT NULL ,
       name           varchar(50) NOT NULL,
       phone          varchar(10)   NULL,
       address_id     bigint        NULL,
	   boss_id        int         NOT NULL,
	   code_edrpou    varchar(10)   NULL,
	   status_id      tinyint     NOT NULL,
	   inserted_date  datetime not null default getdate(),
	   updated_date  datetime not null default getdate(),

----define primary key (table level constraint)
	   constraint provider_pk primary key clustered (provider_id),

----define unique key (alternate key, table level constraint)
	   constraint provider_uk unique (name, boss_id)
)
go


 drop table if exists dbo.supply
go

CREATE TABLE dbo.supply (
       supply_id          bigint IDENTITY (1,1)  NOT NULL ,
       number_bill        varchar(50) NOT NULL,
       [date]             datetime    NOT NULL,
       provider_id        int         NOT NULL,
	   product_id         bigint      NOT NULL,
	   price_incoming     money       NOT NULL,
	   quantity           int         NOT NULL,
	   employee_id        int          NULL,
	   status_id          tinyint     NOT NULL,
	   inserted_date      datetime not null default getdate(),
	   updated_date       datetime not null default getdate(),

--- define default value (row level constraint)
	---   constraint supply_product_id_df default (-1),

--- define check constraint (row level constraint)
	   constraint product_price_incoming_chk check (price_incoming >0) ,

--- define check constraint (row level constraint)
	   constraint product_quantity_chk check (quantity >0) ,

----define primary key (table level constraint)
	   constraint supply_pk primary key clustered (supply_id),

----define unique key (alternate key, table level constraint)
	   constraint supply_uk unique (number_bill, provider_id)
)
go
ALTER TABLE  dbo.supply    
ADD CONSTRAINT [FK_relationship] FOREIGN KEY (product_id)
	REFERENCES dbo.product(product_id) ON DELETE CASCADE ON UPDATE CASCADE ;
GO

ALTER TABLE  dbo.supply    
ADD CONSTRAINT [FK_relationship_2] FOREIGN KEY (provider_id)
	REFERENCES dbo.provider(provider_id) ON DELETE CASCADE ON UPDATE CASCADE ;
GO

-- insert data into product

INSERT INTO dbo.product (product_name, product_photo, product_description, container_id , measure_id , product_type_id, expiration_date , trademark_id, price, status_id )
VALUES ('bolt','image1', 'Bolts with a secret head', 1, 1 , 1 , '2018-12-10' , 3, 1.45,1);
INSERT INTO dbo.product (product_name, product_photo, product_description, container_id , measure_id , product_type_id, expiration_date , trademark_id, price, status_id)
VALUES ('gayka','image2', 'Gayka with a secret head', 1, 1 , 2 , '2019-12-10' , 2, 0.55, 1);
INSERT INTO dbo.product (product_name, product_photo, product_description, container_id , measure_id , product_type_id, expiration_date , trademark_id, price, status_id)
VALUES ('gvynt','image3', 'Gvynt with a secret head', 1, 2 , 1 , '2022-10-10' , 3, 2.05, 2);


-- insert data into provider

INSERT INTO dbo.provider(name, phone, address_id, boss_id, code_edrpou, status_id )
VALUES ('Euroglobal','+310 0105',7, 3, '777898876', 1);
INSERT INTO dbo.provider(name, phone, address_id, boss_id, code_edrpou, status_id)
VALUES ('VASI','+310 0989',7, 5, '777895576', 3);
INSERT INTO dbo.provider(name, phone, address_id, boss_id, code_edrpou, status_id)
VALUES ('Inoxa','+310 0775',2, 3, '777343876', 2);


-- insert data into supply

INSERT INTO dbo.supply (number_bill, [date],provider_id,product_id,price_incoming,quantity, employee_id, status_id)
VALUES (101, '2018-07-15', 1, 1, 456.99, 20, 37, 5);
INSERT INTO dbo.supply (number_bill, [date],provider_id,product_id,price_incoming,quantity, employee_id, status_id)
VALUES (102, '2018-07-20', 2, 1, 878.99, 10, 37, 5);
INSERT INTO dbo.supply (number_bill, [date],provider_id,product_id,price_incoming,quantity, employee_id, status_id)
VALUES (103, '2018-07-22', 1, 3, 1233.99, 10, 37, 3);


-- select test 


select * from dbo.product;
select * from dbo.provider;
select * from dbo.supply;