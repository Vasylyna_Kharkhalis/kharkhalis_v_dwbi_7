use [V_KH_module_3]
go
drop trigger if exists [trg_update_product],[trg_update_provider],[trg_update_supply]
go
create trigger [trg_update_product]
on dbo.product
after update
as
begin
update dbo.product
set [updated_date] = getdate()
where product_id in (select product_id from INSERTED)
end
go
create trigger [trg_update_provider]
on dbo.provider
after update
as
begin
update dbo.provider
set [updated_date] = getdate()
where provider_id in (select provider_id from INSERTED)
end
go
create trigger [trg_update_supply]
on dbo.supply
after update
as
begin
update dbo.supply
set [updated_date] = getdate()
where supply_id in (select supply_id from INSERTED)
end