use master
go
drop database if exists education
go
CREATE DATABASE education
go
use education
go
CREATE SCHEMA [V_Kharkhalis]
go

create synonym V_Kharkhalis.subproduct for V_KH_module_3.dbo.subproduct
create synonym V_Kharkhalis.product_historical for V_KH_module_3.dbo.product_historical
go

select * 
from V_Kharkhalis.subproduct

select * 
from V_Kharkhalis.product_historical  

select count(*) as result
from V_Kharkhalis.product_historical 
where type_operation='insert'

select product_name
from V_Kharkhalis.subproduct
where container_id=1
