use V_KH_module_3
go

drop table if exists dbo.subproduct
go

CREATE TABLE dbo.subproduct (
       product_id          bigint IDENTITY (1,1)  NOT NULL ,
       product_name        varchar(50) NOT NULL,
       product_description varchar(250) NULL,
	   container_id        tinyint     NOT NULL,
	   measure_id          tinyint     NOT NULL,
	   product_type_id     int         NOT NULL,
	   expiration_date     date          NULL,
	   trademark_id        int           NULL,
	   price               money       NOT NULL,
	   status_id           tinyint     NOT NULL,
	   inserted_date        datetime not null default getdate(),
	   updated_date        datetime not null default getdate(),

--- define check constraint (row level constraint)
	   constraint subproduct_price_chk check (price >0) ,

----define primary key (table level constraint)
	   constraint subproduct_pk primary key clustered (product_id),

----define unique key (alternate key, table level constraint)
	   constraint subproduct_uk unique (product_name, trademark_id)
)
go
drop table if exists dbo.product_historical
go
CREATE TABLE dbo.product_historical (
       product_historical_id          bigint IDENTITY (1,1)  NOT NULL ,
	   product_id          bigint  NOT NULL ,
       product_name        varchar(50) NOT NULL,
       product_description varchar(250) NULL,
	   container_id        tinyint     NOT NULL,
	   measure_id          tinyint     NOT NULL,
	   product_type_id     int         NOT NULL,
	   expiration_date     date          NULL,
	   trademark_id        int           NULL,
	   price               money       NOT NULL,
	   status_id           tinyint     NOT NULL,
	   inserted_date       datetime NOT NULL,
	   updated_date        datetime NOT NULL,
	   type_operation      varchar(10) NOT NULL,
	   date_operation      datetime not null default getdate(),


----define primary key (table level constraint)
	   constraint product_historical_pk primary key clustered (product_historical_id),

----define unique key (alternate key, table level constraint)
	   constraint product_historical_uk unique (type_operation, date_operation)
)
go

drop trigger if exists [trg_insert_product],[trg_update_product],[trg_delete_product]
go
create trigger [trg_insert_product]
on dbo.subproduct
after insert
as
begin
declare @type_operation varchar(10) = 'insert'
declare @date_operation datetime = GETDATE();
insert into dbo.product_historical
select *,@type_operation, @date_operation from INSERTED
end
go

create trigger [trg_update_product]
on dbo.subproduct
after update
as
begin
declare @type_operation varchar(10) = 'update'
declare @date_operation datetime = GETDATE();
insert into dbo.product_historical
select *,@type_operation, @date_operation from INSERTED
end
go

create trigger [trg_delete_product]
on dbo.subproduct
after delete
as
begin
declare @type_operation varchar(10) = 'delete'
declare @date_operation datetime = GETDATE();
insert into dbo.product_historical
select *,@type_operation, @date_operation from INSERTED
end
go