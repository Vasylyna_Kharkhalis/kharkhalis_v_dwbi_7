-- transaction b
use education
go

begin transaction

update dbo.details set name = 'DeadLock' where detailid = 1

waitfor delay '00:00:05'
--- Transaction A continue

-- try to upgate 
update dbo.categories set name = 'DeadLock' where id = 1

commit transaction