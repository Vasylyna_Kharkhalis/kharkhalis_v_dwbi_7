-- transaction A
use education
go

begin transaction

update dbo.categories set name = 'DeadLine' where id = 1

waitfor delay '00:00:05'
--- Transaction B started

-- try to Update
update dbo.details set name = 'DeadLine' where detailid = 1


commit transaction