use store
go


begin transaction

select * from dbo.product

---- other statements
---- wait, another transaction updates data
waitfor delay '00:00:10'
---- read data again

select * from dbo.product

commit transaction

---- rollback changes
update dbo.categories 
set name = 'product 1' 
where id = 1
