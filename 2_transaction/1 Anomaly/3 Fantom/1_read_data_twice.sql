use store
go


begin transaction

select * 
from dbo.product  
where id > 3

---- other statements
---- wait, another transaction inserts new row
waitfor delay '00:00:10'
---- read data again


select * 
from dbo.product
where id > 3

commit transaction

---- rollback changes
delete from  dbo.product
where id = 7
