-- first transaction updada data
use store
go

begin transaction

update dbo.product 
set name = 'bolt' 
where id = 1

--wait, first transaction read uncommitted data

waitfor delay '00:00:05'

rollback transaction