-- first transaction updada data
use education
go

begin transaction

update dbo.categories 
set name = 'Clock' 
where id = 1

--wait, first transaction read uncommitted data
waitfor delay '00:00:10'

rollback transaction