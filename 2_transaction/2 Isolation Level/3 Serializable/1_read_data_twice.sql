use education
go

set transaction isolation level serializable

begin transaction

select * 
from dbo.categories  
where id > 3

---- other statements
---- wait, another transaction insert row
waitfor delay '00:00:10'
---- read data again

select * 
from dbo.categories  
where id > 3

commit transaction

---- rollback changes
delete from  dbo.categories 
where id = 10
