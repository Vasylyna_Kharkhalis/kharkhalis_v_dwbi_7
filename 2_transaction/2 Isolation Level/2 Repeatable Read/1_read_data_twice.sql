use education
go

set transaction isolation level repeatable read

begin transaction

select * from dbo.categories

---- other statements
---- wait, another transaction updates data
waitfor delay '00:00:10'
---- read data again

select * from dbo.categories

commit transaction

---- rollback changes
update dbo.categories 
set name = 'category 1' 
where id = 1
