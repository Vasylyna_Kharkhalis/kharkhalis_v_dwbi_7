-----1------------
SELECT DISTINCT maker, [type]
FROM product
WHERE type='Laptop'
ORDER BY maker ASC

-------2----------
SELECT model, ram, screen, price
FROM laptop
WHERE price > 1000
ORDER BY ram ASC, price DESC

-------3-----------
SELECT * FROM printer
WHERE color = 'y'
ORDER BY price DESC

-------4-----------
SELECT model, speed, hd, cd, price
FROM PC
WHERE (cd = '12x' or cd = '24x')
 and price < 600
ORDER BY speed DESC

--------6----------
SELECT * FROM PC
WHERE speed >= 500 and price < 800
ORDER BY price DESC

--------7----------
SELECT * FROM printer
WHERE type != 'Mat' and price < 300 
ORDER BY [type]

--------8----------
SELECT model, speed
FROM PC
WHERE price BETWEEN 400 AND 600 
ORDER BY hd ASC

--------9----------
SELECT model, speed, hd, price
FROM laptop
WHERE screen >= 12
ORDER BY price DESC

--------10---------
SELECT model, [type], price
FROM printer
WHERE price < 300
ORDER BY [type] DESC

--------11---------
SELECT model, ram, price
FROM laptop
WHERE ram = 64
ORDER BY screen ASC

--------12---------
SELECT model, ram, price
FROM laptop
WHERE ram > 64
ORDER BY hd ASC

---------13--------
SELECT model, ram, price
FROM PC
WHERE speed BETWEEN 500 and 750
ORDER BY hd DESC

---------27--------
SELECT model FROM PC
WHERE model like '%1%1%'

--------41---------
SELECT concat('C������ ���� = ',AVG (price)) as avg_price
FROM laptop

--------42---------
SELECT concat('���:', code) as code, 
       concat('������:', model) as model, 
	   concat('���������� ������:', ram) as ram,
	   concat('�������� ����:', hd) as hd,
	   concat('��������:', cd) as cd, 
	   concat('ֳ��:', price) as price
FROM PC

---------48---------
SELECT COUNT(DISTINCT model) as model_count, maker
FROM product
WHERE type='PC'
GROUP BY maker
HAVING COUNT(DISTINCT model)>=2

---------50---------
SELECT COUNT(DISTINCT model) as model_count, [type]
FROM printer
GROUP BY [type]

---------51---------

SELECT COUNT(DISTINCT model) as model_count, cd, 1 AS COUNTER_1
--,  sum(COUNT (cd)) 
FROM PC 
GROUP BY cd


 
