---------14---------
SELECT *
FROM outcome_o
WHERE out > 2000
ORDER BY date DESC

---------15---------
SELECT *
FROM income_o
WHERE inc BETWEEN 5000 AND 10000
ORDER BY inc ASC

----------16---------
SELECT *
FROM income
WHERE point=1
ORDER BY inc ASC

----------17---------
SELECT *
FROM outcome
WHERE point=2
ORDER BY out ASC

----------28----------
SELECT *
FROM outcome
WHERE date BETWEEN '2001-03-01' AND '2001-03-31'

----------29----------
SELECT *
FROM outcome_o
WHERE day (date)=14 

----------43----------
SELECT FORMAT(date, 'yyyy.MM.dd') as date 
FROM income

----------53----------
;with cte as(
SELECT DISTINCT point, date, SUM(out) OVER(partition by point, date) as sum_by_day, 
                             SUM(out) OVER(partition by point) as sum_by_point
FROM outcome) select point, date, sum_by_day, sum_by_point, min(sum_by_day) OVER(partition by point), 
                                     max(sum_by_day) OVER(partition by point) from cte 
GROUP BY point, sum_by_day, sum_by_point, date

