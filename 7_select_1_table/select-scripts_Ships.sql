USE labor_sql
--------------5------------
SELECT name, class
FROM ships
WHERE name=class
ORDER BY name ASC

-------------18-------------
 SELECT *
FROM classes
WHERE country='Japan'
ORDER BY type DESC

--------------19------------
 SELECT  name, launched
FROM ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC

--------------20------------
 SELECT ship, battle, result
FROM outcomes
WHERE battle='Guadalcanal' AND result!='sunk'
ORDER BY ship DESC

--------------21------------
 SELECT ship, battle, result
FROM outcomes
WHERE result='sunk'
ORDER BY ship DESC

 --------------22------------
 SELECT class, displacement
FROM classes
WHERE displacement>=40000
ORDER BY type ASC

 --------------30------------
 SELECT *
FROM ships
WHERE name LIKE 'W%n'

 --------------31------------
 SELECT *
FROM ships
WHERE name LIKE '%e%e%'

 --------------32------------
 SELECT name, launched
FROM ships
WHERE name NOT LIKE '%a'

--------------33------------
 SELECT *
FROM battles
WHERE name  NOT LIKE '%c' AND name LIKE '% %'

--------------44------------
 SELECT *, CASE WHEN result='damaged' THEN 'пошкоджений'
                WHEN result='OK' THEN 'непошкоджений'
			    WHEN result='sunk' THEN 'потоплений' end as ukr
FROM outcomes

