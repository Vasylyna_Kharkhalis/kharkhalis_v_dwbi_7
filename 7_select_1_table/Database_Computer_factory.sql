use master
go
drop database if exists [Computer_factory]
go

CREATE DATABASE [Computer_factory]
go

use Computer_factory
go

drop table if exists dbo.product
go

CREATE TABLE dbo.product (
       maker        varchar(10)  NOT NULL ,
       model        varchar(50)  NOT NULL,
       [type]       varchar(50)  NOT NULL

----define primary key (table level constraint)
	   constraint model_pk primary key nonclustered (model),
)
go

drop table if exists dbo.PC
go

CREATE TABLE dbo.PC (
       code      int   NOT NULL ,
       model    varchar(50)      NOT NULL,
       speed    smallint         NOT NULL,
       ram      smallint         NOT NULL,
	   hd        real            NOT NULL,
	   cd       varchar(10)      NOT NULL,
	   price      money           NULL

----define primary key (table level constraint)
	   constraint code_PC_pk primary key nonclustered (code),
)
go

 drop table if exists dbo.laptop
go

CREATE TABLE dbo.laptop (
       code       int             NOT NULL ,
       model    varchar(50)       NOT NULL,
       speed    smallint          NOT NULL,
       ram      smallint          NOT NULL,
	   hd         real            NOT NULL,
	   price      money             NULL,
	   screen    tinyint           NOT NULL

----define primary key (table level constraint)
	   constraint code_lap_pk primary key nonclustered (code),
)
go

 drop table if exists dbo.printer
go

CREATE TABLE dbo.printer (
       code        int            NOT NULL ,
       model    varchar(50)       NOT NULL,
       color     char(1)          NOT NULL,
       [type]   varchar(10)       NOT NULL,
	   price      money             NULL,

----define primary key (table level constraint)
	   constraint code_print_pk primary key nonclustered (code),
)
go

ALTER TABLE  dbo.PC    
ADD CONSTRAINT [FK_PC] FOREIGN KEY (model)
	REFERENCES dbo.product(model);
GO

ALTER TABLE  dbo.laptop    
ADD CONSTRAINT [FK_laptop] FOREIGN KEY (model)
	REFERENCES dbo.product(model);
GO

ALTER TABLE  dbo.printer    
ADD CONSTRAINT [FK_printer] FOREIGN KEY (model)
	REFERENCES dbo.product(model);
GO

-- insert data into product
INSERT INTO dbo.product (maker, model, [type])
VALUES ('B', '1121','PC');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1232','PC');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1233','PC');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('E', '1260','PC');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1276','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('D', '1288','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1298','Laptop');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('C', '1321','Laptop');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1401','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1408','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('D', '1433','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('E', '1434','Printer');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('B', '1750','Laptop');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('A', '1752','Laptop');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('E', '2111','PC');
INSERT INTO dbo.product (maker, model, [type])
VALUES ('E', '2112','PC')

-- insert data into PC
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (1, '1232', 500, 64, 5,'12x', 600);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (2, '1121', 750, 128, 14,'40x', 800);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (3, '1233', 500, 64, 5,'12x', 600);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (4, '1121', 600, 128, 14,'40x', 850);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (5, '1121', 600, 128, 8,'40x', 850);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (6, '1233', 750, 128, 20,'50x', 950);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (7, '1232', 500, 32, 10,'12x', 400);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (8, '1232', 450, 64, 8,'24x', 350);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (9, '1232', 450, 32, 10,'12x', 350);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (10, '1260', 500, 32, 10,'12x', 350);
INSERT INTO dbo.PC(code, model, speed, ram, hd, cd, price )
VALUES (11, '1233', 900, 128, 40,'40x', 980)

-- insert data into laptop
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (1, '1298', 350, 32, 4, 700, 11);
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (2, '1321', 500, 64, 8, 970, 12);
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (3, '1750', 750, 128, 12, 1200, 14);
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (4, '1298', 600, 64, 10, 1050, 15);
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (5, '1752', 750, 128, 10, 1150, 14);
INSERT INTO dbo.laptop (code, model, speed, ram, hd, price, screen)
VALUES (6, '1298', 450, 64, 10, 950, 12)

-- insert data into printer
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (1, '1276', 'n', 'Laser', 400);
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (2, '1433', 'y', 'Jet', 270);
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (3, '1434', 'y', 'Jet', 290);
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (4, '1401', 'n', 'Matrix', 150);
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (5, '1408', 'n', 'Matrix', 400);
INSERT INTO dbo.printer (code, model, color, [type], price)
VALUES (6, '1288', 'n', 'Laser', 400)

-- select test 

select * from dbo.product;
select * from dbo.PC;
select * from dbo.laptop;
select * from dbo.printer