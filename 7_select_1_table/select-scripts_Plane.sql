-----------23--------------
SELECT  trip_no, town_from, town_to
FROM trip
WHERE town_from='London' OR town_to='LONDON'
ORDER BY time_out ASC

-----------24--------------
SELECT  trip_no, plane, town_from, town_to
FROM trip
WHERE plane='TU-134' 
ORDER BY time_out DESC

-----------25--------------
SELECT   trip_no, plane, town_from, town_to
FROM trip
WHERE plane <> 'IL-86' 
ORDER BY plane ASC

-----------26--------------
SELECT  trip_no, town_from, town_to
FROM trip
WHERE town_from!='Rostov' OR town_to!='Rostov'
ORDER BY plane ASC

-----------34--------------
SELECT *
FROM trip
WHERE DATEPART(HOUR,time_out) BETWEEN 12 AND 17

-----------35--------------
SELECT *
FROM trip
WHERE DATEPART(HOUR,time_in) BETWEEN 17 AND 23

-----------36--------------
SELECT *
FROM trip
WHERE DATEPART(HOUR,time_in) BETWEEN 21 AND 24 OR DATEPART(HOUR,time_in) BETWEEN 00 AND 10

-----------37--------------
SELECT date
FROM Pass_in_trip
WHERE place LIKE '1%'

-----------38--------------
SELECT date
FROM Pass_in_trip
WHERE place LIKE '%c'

-----------39--------------
SELECT *, SUBSTRING (name,PATINDEX('% %',name),len(name)) as last_name
FROM Passenger
WHERE name LIKE '% C%'

-----------40--------------
SELECT *, SUBSTRING (name,PATINDEX('% %',name),len(name)) as last_name
FROM Passenger
WHERE name NOT LIKE '% J%'

-----------45--------------
SELECT *, CONCAT('���:', SUBSTRING (place, 1,1)) as ���,
          CONCAT('̳���:', SUBSTRING (place, 2,1)) as ����
FROM Pass_in_trip

-----------46--------------
SELECT *, CONCAT('from ',RTRIM(town_from), ' to ', town_to) as way
FROM trip

-----------47--------------
SELECT *, CONCAT(SUBSTRING(CAST(trip_no as varchar),1,1), SUBSTRING(CAST(trip_no as varchar),len(trip_no),1)) 
FROM trip

------------49--------------
;with cte1 as (
SELECT DISTINCT town_from, COUNT(trip_no) as g1
FROM trip 
GROUP BY town_from),
cte2 as
(SELECT DISTINCT town_to, COUNT(trip_no) as g2
FROM trip 
GROUP BY town_to)
SELECT *, g1+g2 as fly_count
FROM cte1 JOIN cte2 ON town_from=town_to

------------52--------------
SELECT *, DATEDIFF(minute, time_out, time_in)
FROM trip

------------55--------------
SELECT COUNT(*)
FROM Passenger
WHERE name LIKE '% S%' OR name LIKE '% B%' OR name LIKE '% A%'
