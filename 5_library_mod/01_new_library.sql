use  [V_Kharkhalis_library] 
go
alter table [authors]
add birthday date NULL,
    book_amount int NOT NULL default 0 check (book_amount > = 0),
    issue_amount int NOT NULL default 0 check(issue_amount >= 0),
    total_edition int NOT NULL default 0 check(total_edition >= 0)
go

alter table [BooksAuthors]
drop constraint [fk_book_ISBN]
alter table [books]
drop constraint [pk_books]
go


alter table [books]
add  title varchar(30) NOT NULL  default 'Title',
     edition int NOT NULL  default 1 check(edition >= 1),
     published date NULL,
     issue int NOT NULL  default 0
go

update [books] set issue = 0
alter table [books]
alter column issue int NOT NULL
go
alter table [books]
add constraint [PK_books] primary key ([ISBN],[issue])
go

alter table [publishers]
add  created date        NOT NULL  default '1900-01-01',
     country varchar(40) NOT NULL  default 'USA',
     city    varchar(40) NOT NULL  default 'NY',
     book_amount int     NOT NULL  default 0 check (book_amount >= 0),
     issue_amount int    NOT NULL  default 0 check (issue_amount >= 0),
     total_edition int   NOT NULL  default 0 check (total_edition >= 0)
go

alter table [authors_log]
add   book_amount_old  int NULL,
      issue_amount_old int NULL,
      total_edition_old int NULL,
      book_amount_new   int NULL,
      issue_amount_new  int NULL,
      total_edition_new int NULL
go

alter table [BooksAuthors]
add    issue int NOT NULL default 0
go

alter table [BooksAuthors]
add  constraint [fk_book_ISBN] 
     foreign key(ISBN,issue)
     references [books]([ISBN],[issue])
on update cascade
on delete no action
go

-------------------------------------------

alter trigger [tr_authors_log_insert]
on dbo.authors
after insert
as
BEGIN 
   declare @user varchar(10) = system_user                             
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'I'

	 insert into Authors_log(author_id_new,name_new,URL_new,operation_type,operation_datetime,book_amount_new,issue_amount_new,total_edition_new)  
	        (select author_id,name,URL,@type,@date,book_amount,issue_amount,total_edition from INSERTED)
end
go

alter trigger tr_authors_log_delete
on dbo.authors
after delete
as
BEGIN 
   declare @user varchar(10) = system_user
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'D'

	 insert into Authors_log(author_id_old,name_old,URL_old,operation_type,operation_datetime,book_amount_old,issue_amount_old,total_edition_old)    
	        (select author_id,name,URL,@type,@date,book_amount,issue_amount,total_edition from DELETED) 
	end
go

alter trigger tr_authors_log_update
on dbo.authors
after update
as
BEGIN 
   declare @user varchar(10) = system_user
   declare @date datetime = GETDATE()
   declare @type varchar(1)= 'U'

	update dbo.authors
	set updated = GETDATE(), updated_by = @user
	where author_id in (select author_id from inserted)
	insert into Authors_log(author_id_new,name_new,URL_new,author_id_old,name_old,URL_old,operation_type,operation_datetime,book_amount_new,issue_amount_new,total_edition_new,book_amount_old,issue_amount_old,total_edition_old)
	select i.author_id,i.name,i.URL,d.author_id,d.name,d.URL,@type,@date,i.book_amount,i.issue_amount,i.total_edition,d.book_amount,d.issue_amount,d.total_edition from inserted i,deleted d
	end
go

--- setup environment

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO

-------------------------update authors----------------------------------------------------
UPDATE authors
SET birthday = '1960-09-19', book_amount = 24, issue_amount = 11, total_edition = 100000
WHERE author_id = 1;

UPDATE authors
SET birthday = '1960-03-13', book_amount = 32, issue_amount = 19, total_edition = 320000
WHERE author_id = 2;

UPDATE authors
SET birthday = '1958-07-26', book_amount = 33, issue_amount = 18, total_edition = 650000
WHERE author_id = 3;

UPDATE authors
SET birthday = '1948-10-26', book_amount = 31, issue_amount = 18, total_edition = 650000
WHERE author_id = 4;

UPDATE authors
SET birthday = '1973-06-17', book_amount = 34, issue_amount = 17, total_edition = 630000
WHERE author_id = 5;

UPDATE authors
SET birthday = '1968-12-28', book_amount = 50, issue_amount = 30, total_edition = 660000
WHERE author_id = 6;

UPDATE authors
SET birthday = '1952-11-03', book_amount = 15, issue_amount = 8, total_edition = 610000
WHERE author_id = 7;

UPDATE authors
SET birthday = '1967-07-24', book_amount = 10, issue_amount = 10, total_edition = 610000
WHERE author_id = 8;

UPDATE authors
SET birthday = '1973-09-18', book_amount = 10, issue_amount = 6, total_edition = 590000
WHERE author_id = 9;

UPDATE authors
SET birthday = '1979-12-18', book_amount = 13, issue_amount = 7, total_edition = 580000
WHERE author_id = 10;

UPDATE authors
SET birthday = '1974-08-23', book_amount = 50, issue_amount = 26, total_edition = 600000
WHERE author_id = 11;

UPDATE authors
SET birthday = '1974-05-11', book_amount = 7, issue_amount = 4, total_edition = 570000
WHERE author_id = 12;

UPDATE authors
SET birthday = '1943-08-27', book_amount = 8, issue_amount = 4, total_edition = 550000
WHERE author_id = 13;

UPDATE authors
SET birthday = '1918-09-09', book_amount = 16, issue_amount = 8, total_edition = 530000
WHERE author_id = 14;

UPDATE authors
SET birthday = '1938-10-14', book_amount = 10, issue_amount = 5, total_edition = 510000
WHERE author_id = 15;

UPDATE authors
SET birthday = '1932-03-20', book_amount = 21, issue_amount = 11, total_edition = 500000
WHERE author_id = 16;

UPDATE authors
SET birthday = '1937-12-22', book_amount = 70, issue_amount = 36, total_edition = 520000
WHERE author_id = 17;

UPDATE authors
SET birthday = '1919-09-09', book_amount = 9, issue_amount = 5, total_edition = 540000
WHERE author_id = 18;

UPDATE authors
SET birthday = '1988-04-11', book_amount = 8, issue_amount = 5, total_edition = 490000
WHERE author_id = 19;

UPDATE authors
SET birthday = '1987-05-12', book_amount = 15, issue_amount = 8, total_edition = 470000
WHERE author_id = 20;

---------------------------------update books-------------------------------------
UPDATE books
SET title = '����� ��������: ����� � ���������', edition = 1500, published = '1997', issue = 2
WHERE ISBN = '9789663592374';

UPDATE books
SET title = '������ �������� � ���', edition = 1600, published = '2006', issue = 4
WHERE ISBN = '9668978129';

UPDATE books
SET title = '����� � ���������', edition = 1700, published = '2006', issue = 6
WHERE ISBN = '9668910222';

UPDATE books
SET title = '������', edition = 1400, published = '2006', issue = 8
WHERE ISBN = '9669660106';

UPDATE books
SET title = '���� �� �� ����', edition = 1300, published = '2017', issue = 10
WHERE ISBN = '9789669757418';

UPDATE books
SET title = '����������� ����', edition = 1800, published = '2010', issue = 12
WHERE ISBN = '9789662151633';

UPDATE books
SET title = '������, ��� (���������� ������� �����)', edition = 1900, published = '2015', issue = 14
WHERE ISBN = '9786177279036';

UPDATE books
SET title = '������ 2000', edition = 2000, published = '2001', issue = 16
WHERE ISBN = '9667831043';

UPDATE books
SET title = '����������� �����������', edition = 1700, published = '2008', issue = 18
WHERE ISBN = '9789660345607';

UPDATE books
SET title = '������ �����', edition = 1200, published = '2003', issue = 20
WHERE ISBN = '5733300760';

UPDATE books
SET title = '����������', edition = 2100, published = '2014', issue = 1
WHERE ISBN = '9789661465410';

UPDATE books
SET title = '������� � �������', edition = 1000, published = '2008', issue = 3
WHERE ISBN = '978966146541';

UPDATE books
SET title = '���� �� ����', edition = 1100, published = '2016', issue = 5
WHERE ISBN = '9789661465411';

UPDATE books
SET title = '��������', edition = 1000, published = '1980', issue = 7
WHERE ISBN = '6416638';

UPDATE books
SET title = '��������� ��������, ��� ��� �������� ������', edition = 1000, published = '1986', issue = 9
WHERE ISBN = '0275754';

UPDATE books
SET title = '����� ������ � ����', edition = 1110, published = '2000', issue = 11
WHERE ISBN = '9789669151797';

UPDATE books
SET title = '�������� ���� �� �������', edition = 1250, published = '2004', issue = 13
WHERE ISBN = '5170233027';

UPDATE books
SET title = '������ �������', edition = 2300, published = '1984', issue = 15
WHERE ISBN = '0218940';

UPDATE books
SET title = '����� Գ�� �� ���� �������', edition = 900, published = '2014', issue = 17
WHERE ISBN = '9786177337347';

UPDATE books
SET title = '����� ��� ������� �������', edition = 500, published = '2015', issue = 19
WHERE ISBN = '978617733734';
go


create or alter trigger tr_books
ON dbo.books
after insert
as
begin
update publishers set book_amount = book_amount+1,
issue_amount = issue_amount+1, total_edition=total_edition+(select edition from inserted)
where publisher_id in (select distinct  publisher_id from inserted)
end
go

create trigger tr_booksauthors
ON dbo.booksauthors
after insert
as
begin
update authors set book_amount = book_amount+1, issue_amount = issue_amount+1,
total_edition=total_edition+(select edition from books where ISBN IN (select ISBN FROM inserted))
where author_id in (select distinct  author_id from inserted)
end
go

/*
select * from books
select * from publishers
insert into [Books]([ISBN],[publisher_id],[URL],[price]) values 										   
										   ('9789663592399',1,'www.book.com/21',100)

select * from books;
select * from publishers;
*/
