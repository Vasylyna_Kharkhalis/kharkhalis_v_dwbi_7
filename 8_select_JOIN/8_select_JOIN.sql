---------------1-----------
SELECT  maker, type, speed, hd
FROM product JOIN pc ON product.model=pc.model
WHERE hd<=8

---------------2-----------
SELECT  maker
FROM product JOIN pc ON product.model=pc.model
WHERE speed>=600

---------------3-----------
SELECT  maker
FROM product JOIN laptop ON product.model=laptop.model
WHERE speed<=500

---------------4-----------
SELECT DISTINCT L1.model, L2.model, L1.hd, L1.ram
FROM laptop L1, laptop L2
WHERE (L1.hd=L2.hd OR L1.ram=L2.ram) AND  L1.Model > L2.Model

-------------5-------------
SELECT C1.country, C1.type, C2.type
FROM classes C1, classes C2
WHERE  C1.type='bb' AND C2.type='bc' AND C1.country=C2.country

--------------6-------------
SELECT product.model, maker
FROM product JOIN pc ON product.model=pc.model
WHERE price<600 

--------------7-------------
SELECT product.model, maker
FROM product JOIN printer ON product.model=printer.model
WHERE price>300

-------------8--------------
SELECT pc.model, maker, price
FROM pc JOIN product ON product.model=pc.model

-------------9--------------
SELECT DISTINCT pc.model, maker, price
FROM pc JOIN product ON product.model=pc.model

------------10--------------
SELECT maker, type, laptop.model, speed
FROM laptop JOIN product ON product.model=laptop.model
WHERE speed>600

------------11--------------
SELECT ships.*, displacement
FROM classes JOIN ships ON classes.class=ships.class

------------12--------------
SELECT battles.*, result
FROM outcomes JOIN battles ON outcomes.battle=battles.name
WHERE result!='sunk'

--------------13------------
SELECT ships.*, country
FROM classes JOIN ships ON classes.class=ships.class

------------14--------------
SELECT DISTINCT plane, name
FROM company JOIN trip ON company.id_comp=trip.id_comp
WHERE plane='Boeing'

------------15-------------
SELECT DISTINCT name, date
FROM pass_in_trip JOIN passenger ON pass_in_trip.id_psg=passenger.id_psg

-------------16------------
SELECT pc.model, speed, hd
FROM pc JOIN product ON pc.model=product.model
WHERE (hd=10 OR hd=20) AND maker='A'

------------17-------------
SELECT *
FROM product
PIVOT(COUNT (model) FOR type IN (pc, laptop, printer)) as ups

----------18---------------
SELECT *
FROM (SELECT screen, price, 'average_price' as avg_ FROM laptop) as om
PIVOT(AVG (price) FOR screen IN ([11], [12], [14], [15] )) as ups

----------19---------------
SELECT *
FROM laptop CROSS APPLY 
(SELECT maker FROM product WHERE laptop.model=product.model) product 

----------20----------------
SELECT *
FROM laptop V1 CROSS APPLY
(SELECT MAX(price) as max_price
FROM laptop JOIN product ON laptop.model=product.model
WHERE V1.model=laptop.model
GROUP BY maker) V2

------------21--------------
SELECT * 
FROM laptop L1 CROSS APPLY (SELECT TOP 1 * FROM laptop L2
WHERE L1.model < L2.model OR (L1.model = L2.model AND L1.code < L2.code) 
ORDER BY model, code) L2
ORDER BY L1.model, L1.code

------------22--------------
SELECT * 
FROM laptop L1 OUTER APPLY (SELECT TOP 1 * FROM laptop L2
WHERE L1.model < L2.model OR (L1.model = L2.model AND L1.code < L2.code) 
ORDER BY model, code) L2
ORDER BY L1.model, L1.code

-----------23---------------
SELECT L2.* FROM (SELECT DISTINCT type FROM product) L1
CROSS APPLY (SELECT TOP 3 * FROM product P2 WHERE  L1.type=P2.type ORDER BY P2.model)L2 

-----------24---------------
SELECT code, name, value 
FROM laptop
CROSS APPLY (VALUES ('speed', speed), ('ram', ram), ('hd', hd), ('screen', screen)) char_(name, value)
ORDER BY code, name, value